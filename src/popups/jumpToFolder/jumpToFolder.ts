import { InputProcessor } from "./inputProcessor.js";
import { MailFolderTreeBuilder } from "./mailFolderTreeBuilder.js";


if (!document) {
    throw new Error('document should not be null/undefined!');
}

// set focus to input text field
document.getElementById('filterField')?.focus();

let accounts = await messenger.accounts.list();
await new MailFolderTreeBuilder(document, accounts).build();
registerInputListener();

function registerInputListener() {
    let inputField = document.getElementById('filterField');
    if (!inputField) {
        throw new Error('inputField should not be null/undefined!');
    }
    inputField.oninput = function (e) { new InputProcessor(document, accounts).update() };
}