import { MailFolderTreeNode } from "./mailFolderTreeNode.js";

export class FilterableAccountFolders {
    private filter: string = '';
    private account: messenger.accounts.MailAccount;
    private filteredFolders = new Array<MailFolderTreeNode>;

    constructor(account: messenger.accounts.MailAccount, filter: string) {
        this.account = account;
        this.filter = filter.toLocaleLowerCase();
        this.setFilteredFolders();
    }

    getAccount(): messenger.accounts.MailAccount {
        return this.account;
    }

    getFilteredFolders(): MailFolderTreeNode[] {
        return this.filteredFolders;
    }

    private setFilteredFolders() {
        let allFolders = this.account.folders;
        if (allFolders) {
            this.filteredFolders = this.getFoldersIfVisible(allFolders, 0);
        }
    }

    private getFoldersIfVisible(folders: messenger.folders.MailFolder[], level: number)
        : MailFolderTreeNode[] {
        let visibleFolders = new Array<MailFolderTreeNode>;
        for (let i = 0; i < folders.length; i++) {
            let folder = folders[i];
            if (folder.name && this.isVisible(folder.name)) {
                visibleFolders.push(new MailFolderTreeNode(level, folder.name));
            }
            if (folder.subFolders) {
                visibleFolders = visibleFolders.concat(this.getFoldersIfVisible(folder.subFolders, level + 1));
            }
        }
        return visibleFolders;
    }

    private isVisible(folderName: string): boolean {
        return this.filter === ''
            || folderName.toLocaleLowerCase().includes(this.filter);
    }
}