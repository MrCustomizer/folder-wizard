
export class MailFolderTreeNode {
    private level: number;
    private name: string;

    constructor(level: number, name: string) {
        this.level = level;
        this.name = name;
    }

    getLevel(): number {
        return this.level;
    }

    getName(): string {
        return this.name;
    }
}