import { MailFolderFilter } from "./mailFolderFilter.js";
import { MailFolderTreeNode } from "./mailFolderTreeNode.js";
import { TreeBuilder } from "./treeBuilder.js";

export class MailFolderTreeBuilder {
    private filter = '';
    private document: Document;
    private folderTreeId = 'folderTree';
    private accounts: messenger.accounts.MailAccount[];

    constructor(document: Document, accounts: messenger.accounts.MailAccount[]) {
        this.document = document;
        this.accounts = accounts;
    }

    withFilter(filter: string) {
        this.filter = filter.toLocaleLowerCase();
    }

    build() {
        let builder = new TreeBuilder;

        let filteredAccounts = new MailFolderFilter(this.accounts, this.filter)
            .getFilteredAccountFolders();

        for (let i = 0; i < filteredAccounts.length; i++) {
            let filterableAccount = filteredAccounts[i];
            builder.addNode(filterableAccount.getAccount().name, 'accounts');
            this.processFolders(builder, filterableAccount.getFilteredFolders());
        }

        let treeDiv = document.createElement('div');
        treeDiv.id = this.folderTreeId;
        treeDiv.appendChild(builder.build());
        this.replaceElementInGrid(treeDiv);
    }


    private processFolders(builder: TreeBuilder, folders: MailFolderTreeNode[]) {
        for (let i = 0; i < folders.length; i++) {
            let folder = folders[i];
            builder.switchToLevel(folder.getLevel());
            builder.addNode(folder.getName(), 'folder');
        }
        builder.switchToLevel(0);
    }

    private replaceElementInGrid(element: HTMLElement) {
        let folderGrid = this.document.getElementById('grid');
        if (!folderGrid) {
            throw new Error('folderGrid should not be null/undefined!');
        }

        this.removeTree(folderGrid);
        folderGrid.append(element);
    }

    private removeTree(folderGrid: HTMLElement) {
        let tree = this.document.getElementById(this.folderTreeId);
        if (tree) {
            folderGrid.removeChild(tree);
        }
    }


}
