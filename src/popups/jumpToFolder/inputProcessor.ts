import { MailFolderTreeBuilder } from "./mailFolderTreeBuilder.js";

export class InputProcessor {
    private document: Document;
    private accounts: messenger.accounts.MailAccount[];

    constructor(document: Document, accounts: messenger.accounts.MailAccount[]) {
        this.document = document;
        this.accounts = accounts;
    }

    async update() {
        let inputField = this.document.getElementById('filterField');
        if (!inputField) {
            throw new Error('inputField should not be null/undefined!');
        }
        if (inputField instanceof HTMLInputElement) {
            let builder = new MailFolderTreeBuilder(this.document, this.accounts);
            builder.withFilter(inputField.value);
            builder.build();
        }
    }

}