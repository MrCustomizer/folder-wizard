export class TreeBuilder {
    private tree: HTMLElement;
    private currentLevel = 0;

    private currentLevelNode: HTMLElement;

    constructor() {
        this.tree = document.createElement('ul');
        this.currentLevelNode = this.tree;
    }

    switchToLevel(newLevel: number): number {
        while (this.currentLevel < newLevel) {
            this.currentLevel++;
            this.addLevel();
        }

        while (this.currentLevel > newLevel) {
            this.currentLevel--;
            this.leaveLevel();
        }
        return newLevel;
    }

    private addLevel() {
        let newLevel = document.createElement('ul');
        this.currentLevelNode.append(newLevel);
        this.currentLevelNode = newLevel;
    }

    private leaveLevel() {
        let parent = this.currentLevelNode.parentElement;
        if (!parent) {
            throw new Error('parent should not be null/undefined!');
        }
        this.currentLevelNode = parent;
    }

    addNode(text: string, className: string) {
        let li = document.createElement('li');
        li.className = className;
        li.textContent = text;
        this.currentLevelNode.append(li);
    }

    build(): HTMLElement {
        return this.tree;
    }
}