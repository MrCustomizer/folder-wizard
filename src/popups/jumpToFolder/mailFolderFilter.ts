import { FilterableAccountFolders } from "./filterableAccountFolders.js";

export class MailFolderFilter {

    private accounts = new Array<FilterableAccountFolders>;

    constructor(accounts: messenger.accounts.MailAccount[], filter: string) {
        for (let i = 0; i < accounts.length; i++) {
            this.accounts.push(new FilterableAccountFolders(accounts[i], filter));
        }
    }

    getFilteredAccountFolders(): FilterableAccountFolders[] {
        let accountsWithVisibleFolders = new Array<FilterableAccountFolders>;
        for (let i = 0; i < this.accounts.length; i++) {
            if (this.accounts[i].getFilteredFolders().length > 0) {
                accountsWithVisibleFolders.push(this.accounts[i]);
            }
        }
        return accountsWithVisibleFolders;
    }
}

