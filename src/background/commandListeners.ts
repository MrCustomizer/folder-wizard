let commands = await messenger.commands.getAll();

messenger.commands.onCommand.addListener(handleCommand);

function handleCommand(command: string, tab: messenger.tabs.Tab) {
    if (command === 'jumpToFolder' && tab.mailTab) {
        messenger.windows.create({
            height: 800,
            width: 1000,
            url: "/extension/popups/jumpToFolder/jumpToFolder.html",
            type: "popup"
        });
    }
}

export { };