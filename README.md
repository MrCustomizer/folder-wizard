# Development
Typescript needs the Thunderbird webext-dependencies to be able to compile cleanly. Install them with:
```
npm install --save @types/thunderbird-webext-browser
```